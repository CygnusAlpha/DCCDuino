#include <ArduinoUnit.h>
#include <DCCLineDriver.h>

DCCLineDriver driver;

void setup() {
  Serial.begin(9600);
  Serial.println( "DCCLineDriverTest:" );

}

test(queueLevel_is_0_after_init) {
  driver.init();
  assertEqual( 0, driver.queueLevel() );
}

test(queueLevel_is_1_after_queueMessage) {
  driver.init();
  uint8_t data[5];
 // data=[1,2,3,4,5];
  driver.queueMessage( data );
  assertEqual( 1, driver.queueLevel() );
}

test(compare_dequeued_message_after_queue) {
  driver.init();
  uint8_t data[2];
 // data=[1,2];
  data[0]=0x11;
  data[1]=0x22;
  driver.queueMessage( data );
  data[0]=0x33;
  data[1]=0x44;
  driver.queueMessage( data );
  uint8_t *data2 = driver.dequeueMessage();
  assertEqual( 0x11, data2[0] );
  assertEqual( 0x22, data2[1] );
  data2 = driver.dequeueMessage();
  assertEqual( 0x33, data2[0] );
  assertEqual( 0x44, data2[1] );
}

void loop() {
  Test::run();
}

// '1' bit = first and last part have same duration - 58uS (52-64uS)
// '0' bit = first and last parts greater than or equal to 100uS
//     Each part can have duration between 95uS and 9900uS with total no greater than 12000uS
//     The last part of a '0' bit is normally the same length as the first.
/*
    A preamble of eleven 1's
    An address octet. This is the address of the train you want to control on the layout.
    A command octet. This is 1 bit for direction and 7 bits for speed.
    An error checking octet. This is the address octet XORed with the command octet

Each of these sections is separated by a "0" and the packet ends with a "1" bit.
*/
