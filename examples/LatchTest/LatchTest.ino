#include <Latch.h>
#include <String.h>
#include <ArduinoUnit.h>


Latch latch;

void setup() {
  Serial.begin(9600);
  
  Serial.println( "LatchTest: setup()" );
}

test(latch_state_is_zero_after_init) {
  latch.init();
  assertEqual( 0, latch.get_latch_state() );
}

test(latch_state_is_one_after_set_bit_0) {
  latch.init();
  latch.set_bit(0);
  assertEqual( 1, latch.get_latch_state() );
}

test(latch_state_is_zero_after_clear_bit_0) {
  latch.init();
  latch.clear_bit(0);
  assertEqual( 0, latch.get_latch_state() );
}

test(latch_state_is_one_after_clear_set_bit_0) {
  latch.init();
  latch.clear_bit(0);
  latch.set_bit(0);
  assertEqual( 1, latch.get_latch_state() );
}

test(latch_state_is_zero_after_set_clear_bit_0) {
  latch.init();
  latch.set_bit(0);
  latch.clear_bit(0);
  assertEqual( 0, latch.get_latch_state() );
}

test(latch_state_is_zero_after_shift) {
  latch.init();
  latch.set_bit(0);
  latch.shift();
  assertEqual( 1, latch.get_latch_state() );
}
  
void loop() { 
  // Modulate the N1 outputs
  for( int i=0; i<20; i++) {
    latch.set_bit(3);
    latch.clear_bit(4);
    latch.shift();
    delay(100);
    latch.clear_bit(3);
    latch.set_bit(4);
    latch.shift();
    delay(100);
  }
  latch.clear_bit(3);
  latch.clear_bit(4);
  latch.shift();
  delay(1000);
  // Run the tests
  Test::run();
}

