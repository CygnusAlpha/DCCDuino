// Class:  Latch.h
// Purpose: An interface to the 74HCT595 Shift Register on the Adafruit MotorShield
// Author : B.James custard@cpan.org
// Date   : 17th February 2012
//
// Based on code from:
// Adafruit Motor shield library
// copyright Adafruit Industries LLC, 2009
// this code is public domain, enjoy!

#ifndef _Latch_h_
#define _Latch_h_

#include <inttypes.h>
#include <avr/io.h>

// Arduino pin names
#define STCP 12     // Latch Clock
#define SHCP 4      // Shift Clock
#define OE_ 7       // Output Enable
#define DS 8        // Data

class Latch
{
    public:
        Latch(void);
        void init(void);
        void shift(void) ;
        void set_bit( uint8_t bit );
        void clear_bit( uint8_t bit );
        uint8_t get_latch_state( void );
    private:
        uint8_t latch_data;
};


#endif
