// Class:   DCCLineDriver.cpp
// Purpose: Modulates the line and implements the DCC line comms spec
// Author : B.James custard@cpan.org
// Date   : 20th February 2012
//

#if (ARDUINO >= 100)
 #include "Arduino.h"
#else
 #include <avr/io.h>
 #include "WProgram.h"
#endif

#include "DCCLineDriver.h"


DCCLineDriver* DCCLineDriver::instance = NULL;

DCCLineDriver::DCCLineDriver(void) {
    instance = this;
    init();
}

DCCLineDriver* DCCLineDriver::getInstance( void ) {
    if (instance == NULL) {
        instance = new DCCLineDriver();
    }
    return instance;
}


void DCCLineDriver::init( void ) {
    queue_head=0;
    queue_tail=0;
    setupTimer2();
    latch.init();
}

uint8_t DCCLineDriver::queueMessage( uint8_t *msg ) {
    while (locked);
    locked=true;
    uint8_t tail = (queue_tail + 1) % QUEUE_MAX;
    if (tail == queue_head) {
        // Buffer full
        locked=false;
        return 0;
    } else {
        uint8_t tmp[QUEUE_MAX_MESSAGE_LEN];
        for( uint8_t i=0; i<QUEUE_MAX_MESSAGE_LEN; i++) {
            tmp[i] = msg[i];
        }
        queue[queue_tail] = tmp;
        queue_tail = tail;
        locked=false;
        return 1;
    }
}

uint8_t DCCLineDriver::queueLevel( void ) {
    // Return number of elements in the queue.
    return queue_tail - queue_head + (queue_head > queue_tail ? QUEUE_MAX : 0);
}

void DCCLineDriver::setupTimer2( void ){
    // Setup timer2 to generate a TIMER2_COMPA_vect interrupt for each bit (58uS)

    //Timer2 Settings: Timer Prescaler /8, mode 0
    TCNT1 = 0;
    //Timer clock = 16MHz/8 = 2MHz / 0.5uS
    TCCR2A = 0;
    TCCR2B = 1<<WGM12|0<<CS12 | 1<<CS11 | 0<<CS10; // b00000010

    //Enable Timer2 interrupt 
    TIMSK2 = 1<<OCIE2A;

    // Load the counter target
    OCR2A = TIMER_58uS;
}

ISR( TIMER2_COMPA_vect ) {
    // Timer2 Comparator Interrupt handler. Called every 58uS
    static uint8_t durationCount=0;
    static bool startCycle=true;
    static bool bitValue=false;

    DCCLineDriver *driver = DCCLineDriver::getInstance();

    if (durationCount==0) {
        if (startCycle) {
            bitValue = driver->getNextBit();
            driver->lineLow(0);
        } else {
            driver->lineHigh(0);
        }
        durationCount = (bitValue) ? 1 : 2;
        startCycle=startCycle^1;
    }    
    durationCount--;
}

uint8_t* DCCLineDriver::dequeueMessage( void ) {
    // Get something from the queue. idle_message will be returned if the queue is empty
    while(locked);
    locked=true;
    static uint8_t idle_message[QUEUE_MAX_MESSAGE_LEN];
    idle_message[0]=0xFF;
    idle_message[1]=0x00;
    if (queue_head == queue_tail) {
            locked=false;
            return idle_message;
    } else {
        uint8_t msg[QUEUE_MAX_MESSAGE_LEN];
        for( uint8_t i=0; i<QUEUE_MAX_MESSAGE_LEN; i++) {
            msg[i] = (queue[queue_head])[i];
        }
        queue_head  = (queue_head + 1) % QUEUE_MAX;
        locked=false;
        return msg;
   }
} 


bool DCCLineDriver::getNextBit() {
    static uint8_t *data;
    static uint8_t dataBit=10;
    static uint8_t dataByte=0;
    static uint8_t byte=0;
    static uint8_t checksum=0;
    static uint8_t state=PREAMBLE;
    static uint8_t count=1;
    bool bitValue;

    switch(state) {
        case PREAMBLE : 
            bitValue=true;              // Send the Preamble 1
            if (dataBit==0) {           // Last bit of the preamble
                data=dequeueMessage();  // Fetch the next data message
                dataByte=0;             // Start at the first byte
                checksum=0;
                state=SEPARATOR;
            } else {
                dataBit--;
            }
            break;
        case SEPARATOR : 
            bitValue=false;         // Send the separator 0
            dataBit=7;              // Start at the MSB
            if (dataByte<QUEUE_MAX_MESSAGE_LEN) {
                // Get next byte to transmit
                byte = data[dataByte];
                checksum = checksum ^ byte;
                state=DATABYTE;
            } else {
                state=CHECKSUM;
            }
            break;
        case DATABYTE :
            bitValue = (byte & _BV(dataBit));
            if (dataBit==0) {       // Finished the byte
                dataByte++;         // Move to the next byte
                state=SEPARATOR;
            } else {
                dataBit--;
            }
            break;
        case CHECKSUM :
            bitValue = (checksum & _BV(dataBit));
            if (dataBit==0) {       // Finished the byte
                state=STOPBIT;
            } else {
                dataBit--;
            }
            break;
        case STOPBIT :
            bitValue=true;
            dataBit=10;
            state=PREAMBLE;
            count=1;
            break;
    }
    return bitValue;
}

void DCCLineDriver::lineHigh( uint8_t line ) {
    // line = 0,1,2,3
    uint8_t a = line * 2;   //MxA
    uint8_t b = a+1;        //MxB
    latch.set_bit(3);
    latch.clear_bit(4);
    latch.shift();
}

void DCCLineDriver::lineLow( uint8_t line ) {
    // line = 0,1,2,3
    uint8_t a = line * 2;   //MxA
    uint8_t b = a+1;        //MxB
    latch.clear_bit(3);
    latch.set_bit(4);
    latch.shift();
}


