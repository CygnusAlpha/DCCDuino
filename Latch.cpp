// Class:  Latch.cpp
// Purpose: An interface to the 74HCT595 Shift Register on the Adafruit MotorShield
// Author : B.James custard@cpan.org
// Date   : 17th February 2012
//
// Based on code from:
// Adafruit Motor shield library
// copyright Adafruit Industries LLC, 2009
// this code is public domain, enjoy!


#if (ARDUINO >= 100)
 #include "Arduino.h"
#else
 #include <avr/io.h>
 #include "WProgram.h"
#endif

#include "Latch.h"

Latch::Latch(void) {
    init();
}

void Latch::init(void) {
    pinMode(STCP, OUTPUT);
    pinMode(OE_, OUTPUT);
    pinMode(DS, OUTPUT);
    pinMode(SHCP, OUTPUT);

    // Clear the shift register
    latch_data=0;
    shift();

    digitalWrite(OE_, LOW);
}


void Latch::shift(void) {
    uint8_t i;

    // Disable the latch
    digitalWrite(STCP, LOW);
    digitalWrite(DS, LOW);

    // Shift in new data
    for (i=8; i>0; i--) {
        digitalWrite(SHCP, LOW);

        if (latch_data & _BV(i)) {
          digitalWrite(DS, HIGH);
        } else {
          digitalWrite(DS, LOW);
        }
        digitalWrite(SHCP, HIGH);
    }

    // Enable the latch
    digitalWrite(STCP, HIGH);
}

uint8_t Latch::get_latch_state( void ) {
    return latch_data;
}
void Latch::set_bit( uint8_t bit ) {
    latch_data |= _BV(bit);
}

void Latch::clear_bit( uint8_t bit ){
    latch_data &= ~_BV(bit);
}
