// Class:   DCCLineDriver.h
// Purpose: Modulates the line and implements the DCC line comms spec
// Author : B.James custard@cpan.org
// Date   : 20th February 2012
//

#ifndef _DCCLineDriver_h_
#define _DCCLineDriver_h_

#include <inttypes.h>
#include <avr/io.h>
#include "Latch.h"

//Timer frequency is 2MHz, 0.5uS ( 16MHz / 8 prescale ) 
#define TIMER_58uS  0x74  // 58uS (116 * 0.5uS)

// Queue
#define QUEUE_MAX 10            // Max Number of items in the queue
#define QUEUE_MAX_MESSAGE_LEN 2 // Max number of bytes in a message (not inc preamble/checksum )

// States
#define PREAMBLE 1
#define DATABYTE 2
#define SEPARATOR 3
#define CHECKSUM 4
#define STOPBIT 5

 
 
class DCCLineDriver {
    public:
        DCCLineDriver( void );
        static DCCLineDriver* getInstance();
        void init( void );

        uint8_t queueLevel( void );
        uint8_t queueMessage( uint8_t *message );
        uint8_t* dequeueMessage( void );

        void lineHigh( uint8_t line );
        void lineLow( uint8_t line );

        bool getNextBit();

    protected:
       

    private:
        static DCCLineDriver* instance;
        void setupTimer2( void );
        Latch latch;
        volatile uint8_t queue_head;            // Next item to dequeue
        volatile uint8_t queue_tail;            // Next available queue slot
        volatile bool locked;
        uint8_t *queue[QUEUE_MAX];
 };


#endif
